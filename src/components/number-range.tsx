import {defineComponent} from 'vue';
import {useModel} from "@/packages/utils/useModel";
import './number-range.scss'

export const NumberRange = defineComponent({
    props: {
        start: {},
        end: {},
        width: {type: Number},
    },
    emits: {
        'update:start': (val: any) => true,
        'update:end': (val: any) => true,
    },
    setup(props, ctx) {

        const startModel = useModel(() => props.start, val => ctx.emit('update:start', val))
        const endModel = useModel(() => props.end, val => ctx.emit('update:end', val))

        return () => (
            <div class="number-range" style={`width:${props.width}px`}>
                <input type="text" v-model={startModel.value}/>
                <i>~</i>
                <input type="text" v-model={endModel.value}/>
            </div>
        )
    },
})