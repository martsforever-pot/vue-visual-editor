import {defineComponent, getCurrentInstance, PropType, reactive, watch} from 'vue';
import {VisualEditorBlock, VisualEditorModelValue, VisualEditorOption} from "@/packages/editor.utils";
import {ElButton, ElCheckbox, ElColorPicker, ElForm, ElFormItem, ElInput, ElInputNumber, ElOption, ElSelect} from "element-plus";
import deepcopy from 'deepcopy';
import {VisualEditorProp, VisualEditorPropType} from "@/packages/editor.props";
import {TablePropEditor} from "@/packages/components/table-prop-editor/table-prop-editor";

export interface VueVisualEditorOperatorInstance {
    apply: () => void,
    reset: () => void,
}

export const VueVisualEditorOperator = defineComponent({
    props: {
        selectBlocks: {type: Object as PropType<VisualEditorBlock | null>},
        editorValue: {type: Object as PropType<VisualEditorModelValue>, required: true},
        option: {type: Object as PropType<VisualEditorOption>, required: true}
    },
    emits: {
        updateModelValue: (val: VisualEditorModelValue) => true,
        updateBlock: (newBlock: VisualEditorBlock, oldBlock: VisualEditorBlock) => true,
    },
    setup(props) {

        const ctx = getCurrentInstance()!

        const state = reactive({
            editData: null as any,
        })

        const handler = {
            apply: () => {
                !props.selectBlocks ?
                    ctx.emit('updateModelValue', {
                        ...props.editorValue,
                        container: state.editData,
                    })
                    : ctx.emit('updateBlock', state.editData, props.selectBlocks)
            },
            reset: () => {
                if (!!props.selectBlocks) {
                    state.editData = deepcopy(props.selectBlocks)
                    if (!state.editData.props) {
                        state.editData.props = {}
                    }
                    if (!state.editData.model) {
                        state.editData.model = {}
                    }
                } else {
                    state.editData = deepcopy(props.editorValue.container)
                }
            },
        }
        Object.assign(ctx.proxy, handler)

        watch(() => props.selectBlocks, handler.reset, {immediate: true})

        const renderMap = {
            [VisualEditorPropType.text]: (propName: string, config: VisualEditorProp) => (
                <ElFormItem label={config.label}>
                    <ElInput v-model={state.editData.props[propName]}/>
                </ElFormItem>
            ),
            [VisualEditorPropType.color]: (propName: string, config: VisualEditorProp) => (
                <ElFormItem label={config.label}>
                    <ElColorPicker v-model={state.editData.props[propName]}/>
                </ElFormItem>
            ),
            [VisualEditorPropType.select]: (propName: string, config: VisualEditorProp) => (
                <ElFormItem label={config.label}>
                    <ElSelect v-model={state.editData.props[propName]}>
                        {config.options!.map(opt => (
                            <ElOption label={opt.label} value={opt.val}/>
                        ))}
                    </ElSelect>
                </ElFormItem>
            ),
            [VisualEditorPropType.model]: (modifier: string, config: VisualEditorProp) => <>
                <ElFormItem label={config.label}>
                    <ElInput v-model={state.editData.model[modifier]}/>
                </ElFormItem>
            </>,
            [VisualEditorPropType.boolean]: (propName: string, config: VisualEditorProp) => (
                <ElFormItem label={config.label}>
                    <ElCheckbox v-model={state.editData.props[propName]}>
                        {state.editData.props[propName] ? '是' : '否'}
                    </ElCheckbox>
                </ElFormItem>
            ),
            [VisualEditorPropType.table]: (propName: string, config: VisualEditorProp) => (
                <ElFormItem label={config.label}>
                    <TablePropEditor v-model={state.editData.props[propName]} config={config} {...{onChange: handler.apply} as any}/>
                </ElFormItem>
            ),
        }

        return () => {
            let EditProp: any
            let EditModel: any

            if (!state.editData) {
                return null
            }

            if (!props.selectBlocks) {
                EditProp = <>
                    <ElFormItem label="容器宽度">
                        <ElInputNumber v-model={state.editData.width} step={100} min={0} precision={0}/>
                    </ElFormItem>
                    <ElFormItem label="容器高度">
                        <ElInputNumber v-model={state.editData.height} step={100} precision={0}/>
                    </ElFormItem>
                </>
            } else {
                const Component = props.option.componentMap[props.selectBlocks.componentKey]
                if (!!state.editData.props) {
                    EditProp = <>
                        {Object
                            .entries(Component.props || {})
                            .map(([propName, propConfig]) => renderMap[propConfig.type](propName, propConfig))}
                    </>
                }
                if (!!Component.model) {
                    EditModel = <>
                        {Object.entries(Component.model).map(([modifier, config]) => {
                            return renderMap[config.type](modifier, config)
                        })}
                    </>
                }
            }

            return (
                <div class="vue-visual-operator">
                    <div>
                        {!!props.selectBlocks ? '组件设置' : '容器设置'}
                    </div>
                    <ElForm>
                        {!!props.selectBlocks && (
                            <ElFormItem label="组件标识">
                                <ElInput v-model={state.editData.slotName}/>
                            </ElFormItem>
                        )}
                        {!!EditModel && <EditModel/>}
                        {!!EditProp && <EditProp/>}
                        <ElFormItem>
                            <ElButton type="primary" {...{onClick: handler.apply} as any}>应用</ElButton>
                            <ElButton {...{onClick: handler.reset} as any}>重置</ElButton>
                        </ElFormItem>
                    </ElForm>
                </div>
            )
        }
    },
})