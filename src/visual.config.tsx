import './visual.config.scss'
import {createVisualEditorOption} from "@/packages/editor.utils";
import {ElButton, ElCheckbox, ElDatePicker, ElInput, ElInputNumber, ElOption, ElRadio, ElSelect, ElSwitch,} from 'element-plus'
import {createBooleanProp, createColorProp, createModelProp, createSelectProp, createTableProp, createTextProp} from "@/packages/editor.props";
import {NumberRange} from "@/components/number-range";

export const visualEditorBaseOption = createVisualEditorOption()

visualEditorBaseOption.registryComponent('text', {
    name: '文本',
    render: ({props}) => <span style={{color: props.color, fontSize: props.size}}>{props.text || '文本'}</span>,
    preview: () => (
        <div style="text-align:center;font-size:14px">
            普通文本
        </div>
    ),
    props: {
        text: createTextProp({
            label: '显示文本',
            default: '普通文本',
        }),
        color: createColorProp({
            label: '字体颜色',
        }),
        size: createSelectProp({
            label: '字体大小',
            options: [
                {label: '12px', val: '12px'},
                {label: '14px', val: '14px'},
                {label: '16px', val: '16px'},
            ]
        })
    },
})

visualEditorBaseOption.registryComponent('number-range', {
    name: '数字范围',
    render: ({model}) => {
        return (
            <NumberRange
                style={`width:${225}px`}
                {...model.start}
                {...model.end}
            />
        )
    },
    preview: () => (
        <NumberRange class="visual-block-number-range"/>
    ),
    model: {
        start: createModelProp({label: '起始输入框绑定字段'}),
        end: createModelProp({label: '结尾输入框绑定字段'}),
    },
})

visualEditorBaseOption.registryComponent('switch', {
    name: '开关按钮',
    render: ({props, model}) => <ElSwitch
        {...model.modelValue}
        activeText={props.activeText}
        inactiveText={props.inactiveText}
        activeColor={props.activeColor}
        inactiveColor={props.inactiveColor}
    />,
    preview: () => (
        <div style="text-align:center">
            <el-switch/>
        </div>
    ),
    model: {
        modelValue: createModelProp({label: '绑定字段',}),
    },
    props: {
        activeText: createTextProp({label: '开启时文字描述'}),
        inactiveText: createTextProp({label: '关闭时文字描述'}),
        activeColor: createColorProp({label: '开启颜色'}),
        inactiveColor: createColorProp({label: '关闭颜色'}),
    },
})

visualEditorBaseOption.registryComponent('button', {
    name: '按钮',
    render: ({props, custom, block}) => (
        <ElButton
            style={{
                width: `${block.width || '68'}px`,
                height: `${block.height || '32'}px`,
            }}
            type={props.type}
            size={props.size || 'mini'}
            plain={props.plain}
            {...custom}
        >
            {props.label || '按钮'}
        </ElButton>
    ),
    resize: {
        height: true,
        width: true,
    },
    preview: () => (
        <div style="text-align:center">
            <ElButton size="mini">主要按钮</ElButton>
        </div>
    ),
    props: {
        label: createTextProp({label: '按钮文本'}),
        plain: createBooleanProp({label: '是否为朴素按钮'}),
        type: createSelectProp({
            label: '按钮类型',
            options: [
                {label: '基础', val: 'primary'},
                {label: '成功', val: 'success'},
                {label: '信息', val: 'info'},
                {label: '警告', val: 'warning'},
                {label: '危险', val: 'error'},
            ]
        }),
        size: createSelectProp({
            label: '大小尺寸',
            options: [
                {label: '大', val: ''},
                {label: '中', val: 'medium'},
                {label: '小', val: 'small'},
                {label: '极小', val: 'mini'},
            ]
        }),
    },
})

visualEditorBaseOption.registryComponent('image', {
    name: '图片',
    resize: {
        width: true,
        height: true,
    },
    render: ({props, block}) => {
        return (
            <div style={{height: `${block.height || 100}px`, width: `${block.width || 100}px`}} class="visual-block-image">
                <img src={props.url || 'https://cn.vuejs.org/images/logo.png'}/>
            </div>
        )
    },
    preview: () => (
        <div style="text-align:center;">
            <div style="font-size:20px;background-color:#f2f2f2;color:#ccc;display:inline-flex;width:100px;height:50px;align-items:center;justify-content:center">
                <i class="el-icon-picture"/>
            </div>
        </div>
    ),
    props: {
        url: createTextProp({label: '地址'})
    },
})

visualEditorBaseOption.registryComponent('input', {
    name: '输入框',
    resize: {
        width: true,
    },
    render: ({model, custom, block}) => {
        return <ElInput style={`width:${block.width || 225}px`} {...model.modelValue}{...custom}/>
    },
    preview: () => (
        <div style="text-align:center">
            <el-input size="mini"/>
        </div>
    ),
    model: {
        modelValue: createModelProp({label: '绑定字段',}),
    },
})

visualEditorBaseOption.registryComponent('radio', {
    name: '单选框',
    render: () => (
        <ElRadio>
            单选选项
        </ElRadio>
    ),
    preview: () => (
        <div style="text-align:center">
            <el-radio>单选选项</el-radio>
        </div>
    )
})

visualEditorBaseOption.registryComponent('checkbox', {
    name: '多选框',
    render: () => (
        <ElCheckbox>
            多选选项
        </ElCheckbox>
    ),
    preview: () => (
        <div style="text-align:center">
            <el-checkbox>多选选项</el-checkbox>
        </div>
    )
})

visualEditorBaseOption.registryComponent('number', {
    name: '计数器',
    render: ({props, model}) => <ElInputNumber style={`width:${props.width || 225}px`} {...model.modelValue}/>,
    preview: () => (
        <div style="text-align:center">
            <el-input-number controls-position="right" style="width:100%" size="mini"/>
        </div>
    ),
    model: {
        modelValue: createModelProp({label: '绑定字段',}),
    },
})

visualEditorBaseOption.registryComponent('select', {
    name: '下拉选择',
    render: ({props, model}) => {
        return (
            <ElSelect
                key={!props.options ? '0' : props.options.length}
                style={`width:${225}px`}
                {...(!!model.modelValue ? model.modelValue : {modelValue: null})}>
                {(props.options || []).map((opt: any, i: number) => <ElOption label={opt.label} value={opt.value} key={i}/>)}
            </ElSelect>
        )
    },
    preview: () => (
        <div style="text-align:center">
            <el-select placeholder="请选择" modelValue={"dangao"} size="mini">
                <el-option label="蛋糕" value="dangao"/>
                <el-option label="生煎" value="shengjian"/>
            </el-select>
        </div>
    ),
    model: {
        modelValue: createModelProp({label: '绑定字段',}),
    },
    props: {
        options: createTableProp({
            label: '下拉选项',
            showField: 'label',
            columns: [
                {label: '显示值', field: 'label'},
                {label: '绑定值', field: 'value'},
            ],
        })
    },
})

visualEditorBaseOption.registryComponent('datepicker', {
    name: '日期选择',
    render: ({props, model}) => <ElDatePicker style={`width:${props.width || 225}px`} {...model.modelValue}/>,
    preview: () => (
        <div style="text-align:center">
            <el-date-picker type="date" placeholder="选择日期" style="width:100%" size="mini"/>
        </div>
    ),
    model: {
        modelValue: createModelProp({label: '绑定字段',}),
    },
})